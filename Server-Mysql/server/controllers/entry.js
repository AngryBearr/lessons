var _ = require('underscore');
var Uuid = require('node-uuid');
var mysql = require('mysql');


function dbConnect(str, obj, fun) {
	var connection = mysql.createConnection({
		host: "127.0.0.1",
		user: "root",
		password: "451245",
		database: "userDB"
	});

	connection.query(str, obj, fun);
}


function myError (code, msg) {
	return {code: code, msg: msg};
}


function getAllEntries(cb) {

	dbConnect("SELECT * FROM dataDB", function(error, result, fields) {
		cb(error, result);
	});
}


exports.getAllEntriesApi = function getAllEntriesApi (req, res) {
	getAllEntries(function (err, data) {
		if (err) {
			res.status(err.code).send(err.msg);
		} else {
			res.header('Content-Type', 'application/json');
			res.send(JSON.stringify(data));
		}
	});
}


function getEntryById (EntryId, cb) {

	dbConnect("SELECT * FROM dataDB WHERE EntryId = " + EntryId, function(error, result) {
		if (error) {
			return cb(myError(401, 'Entry with id '+ EntryId + ' not exist'));
		}
		cb(error, result);
	});
}


exports.getEntryByIdApi = function getEntryByIdApi (req, res) {
	getEntryById(req.params.EntryId, function (err, data) {
		if (err) {
			res.status(err.code).send(err.msg);
		} else {
			res.header('Content-Type', 'application/json');
			res.send(data);
		}
	});
}


function addEntry(body, cb) {
	if (!body.Author) {
		return cb(myError(409, 'Author is mandatory'));
	}
	if (!body.Title) {
		return cb(myError(409, 'Title is mandatory'));
	}
	if (!body.Text) {
		return cb(myError(409, 'Text is mandatory'));
	}

	var EntryId = Uuid.v4();

	var Entry = {
		EntryId: EntryId,
		Author: body.Author,
		Title: body.Title,
		Text: body.Text,
		DateCreated: Date.now(),
		DateModified: Date.now()
	};

	dbConnect("INSERT INTO dataDB SET ?", Entry, function(error, result, fields){
		cb(error, result);
	});
}


exports.addEntryApi = function addEntryApi (req, res) {
	addEntry(req.body, function (err, data) {
		if (err) {
			res.status(err.code).send(err.msg);
		} else {
			res.send(data);
		}
	});
}


function updateEntry (body, cb) {
	if (!body.EntryId) {
		return cb(myError(409, 'EntryId is mandatory'));
	}
	if (!body.Author) {
		return cb(myError(409, 'Author is mandatory'));
	}
	if (!body.Title) {
		return cb(myError(409, 'Title is mandatory'));
	}
	if (!body.Text) {
		return cb(myError(409, 'Text is mandatory'));
	}

	var Entry = {};

	dbConnect("UPDATE dataDB SET Author = ?, Title = ?, Text = ?, DateModified = ? WHERE EntryId = ?",[body.Author, body.Title, body.Text, Date.now(), body.EntryId], function(error, result) {
		if (error) {
			return cb(myError(401, 'Failed to update entry'));
		}

		cb(null, 'Entry ' + body.Title + ' was successfully updated');
	});
}


exports.updateEntryApi = function updateEntryApi (req, res) {
	updateEntry(req.body, function (err, data) {
		if (err) {
			res.status(err.code).send(err.msg);
		} else {
			res.send(data)
		}
	});
}


function  deleteEntry(body, cb) {
	if (!body.EntryId) {
		return cb(myError(409, 'EntryId is mandatory'));
	}
	dbConnect("DELETE FROM dataDB WHERE EntryId = ?", body.EntryId, function(error, result) {
		if (error) {
			return cb(myError(401, 'Failed to delete entry'));
		}
		cb(null, 'Entry with id ' + body.EntryId + ' was successfully deleted');
	});
}

exports.deleteEntryApi = function deleteEntryApi (req, res) {
	deleteEntry(req.body, function (err, data) {
		if (err) {
			res.status(err.code).send(err.msg);
		} else {
			res.send(data);
		}
	});
}
