(function () {
	angular
		.module('app')
		.config(routes);

		function routes ($stateProvider, $urlRouterProvider) {
			$stateProvider
				.state('main', {
					url:'/main',
					controller: 'MainCtrl',
					controllerAs: 'main',
					templateUrl: '/app/components/main/templates/main.tpl.html'
				})
				.state('main.home', {
					url:'/home',
					controller: 'HomeCtrl',
					controllerAs: 'home',
					templateUrl: '/app/components/home/templates/home.tpl.html'
				})
				.state('main.blog', {
					url:'/blog',
					controller: 'BlogCtrl',
					controllerAs: 'blog',
					templateUrl: '/app/components/blog/templates/blog.tpl.html'
				})
				.state('main.comments', {
					url:'/comments',
					controller: 'CommetsCtrl',
					controllerAs: 'comments',
					templateUrl: '/app/components/comments/templates/comments.tpl.html'
				});

			$urlRouterProvider.otherwise('main');
		}
}());