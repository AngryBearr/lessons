(function () {
	angular
		.module('app')
		.controller('BlogCtrl', [BlogCtrl]);

	function BlogCtrl () {
		var self = this;

		self.posts = [
			{
				img:'http://fakeimg.pl/350x200/?text=Hello',
				text:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis eos molestiae, quas natus inventore delectus rem ea veritatis laborum, alias commodi aperiam harum accusantium incidunt consequuntur? Fugit, earum porro adipisci iure ratione nam aliquam sit veniam ullam, consequatur est saepe quis nemo error, at alias ad explicabo eaque sunt facilis.'
			},
			{
				img:'http://fakeimg.pl/350x200/?text=Bye',
				text:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis eos molestiae, quas natus inventore delectus rem ea veritatis laborum, alias commodi aperiam harum accusantium incidunt consequuntur? Fugit, earum porro adipisci iure ratione nam aliquam sit veniam ullam, consequatur est saepe quis nemo error, at alias ad explicabo eaque sunt facilis.'
			},
			{
				img:'http://fakeimg.pl/350x200/?text=Privet',
				text:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis eos molestiae, quas natus inventore delectus rem ea veritatis laborum, alias commodi aperiam harum accusantium incidunt consequuntur? Fugit, earum porro adipisci iure ratione nam aliquam sit veniam ullam, consequatur est saepe quis nemo error, at alias ad explicabo eaque sunt facilis.'
			},
			{
				img:'http://fakeimg.pl/350x200/?text=Poka',
				text:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis eos molestiae, quas natus inventore delectus rem ea veritatis laborum, alias commodi aperiam harum accusantium incidunt consequuntur? Fugit, earum porro adipisci iure ratione nam aliquam sit veniam ullam, consequatur est saepe quis nemo error, at alias ad explicabo eaque sunt facilis.'
			},
			{
				img:'http://fakeimg.pl/350x200/?text=Aloha',
				text:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis eos molestiae, quas natus inventore delectus rem ea veritatis laborum, alias commodi aperiam harum accusantium incidunt consequuntur? Fugit, earum porro adipisci iure ratione nam aliquam sit veniam ullam, consequatur est saepe quis nemo error, at alias ad explicabo eaque sunt facilis.'
			},
			{
				img:'http://fakeimg.pl/350x200/?text=Ciao',
				text:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis eos molestiae, quas natus inventore delectus rem ea veritatis laborum, alias commodi aperiam harum accusantium incidunt consequuntur? Fugit, earum porro adipisci iure ratione nam aliquam sit veniam ullam, consequatur est saepe quis nemo error, at alias ad explicabo eaque sunt facilis.'
			}
			];

		self.msg = "BlogCtrl";

		self.addFunc = function () {
			var newPostObj = {
				img: self.imgLink,
				text: self.postBody
			};

			self.posts.push(newPostObj);

			self.imgLink = '';
			self.postBody = '';
		};
	}
}());