(function () {
	angular
		.module('app')
		.controller('CommetsCtrl', [CommetsCtrl]);

		function CommetsCtrl () {
			var self = this;

			self.addedComments = [
				{
					commentName: 'Some Comment',
					commentBody: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea consectetur ipsa architecto eligendi sit est maxime, velit quam cum officiis error omnis, doloribus, sed itaque commodi laborum! Ullam nulla labore quidem accusantium expedita magnam dignissimos laborum, incidunt quod beatae quo recusandae voluptates omnis accusamus molestias iste, pariatur qui. Delectus, reiciendis.'
				},
				{
					commentName: 'Some Comment1',
					commentBody: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, delectus fugit fugiat nobis totam assumenda aliquam aspernatur mollitia laudantium natus quidem libero dolor consequuntur cum ad, consectetur molestias! Excepturi, fugiat.'
				}
			];

			self.addFunc = function addFunc () {
				var commentObj = {
					commentName: self.commentName,
					commentBody: self.commentBody
				};

				self.addedComments.push(commentObj);

				self.commentName = '';
				self.commentBody = '';
			};

			self.delFunc = function (i) {
				self.addedComments.splice(i, 1);
			};
		}
}());