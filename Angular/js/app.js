angular.module("app", []);

(function() {
	"use strict";

	angular.module("app").controller("calcCtrl", [calcCtrl]);

	function calcCtrl() {
		var self = this;

		self.doThat = function (char) {
			if(char === "+") {
				self.res = self.input1 + self.input2;
			}
			if(char === "-") {
				self.res = self.input1 - self.input2;
			}
			if(char === "*") {
				self.res = self.input1 * self.input2;
			}
			if(char === "/") {
				self.res = self.input1 / self.input2;
			}
		}

	}

})();