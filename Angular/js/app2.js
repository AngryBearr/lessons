angular.module("app", []);

(function() {
	angular.module("app").controller("MainCtrl", [MainCtrl]);

	function MainCtrl() {
		var self = this;

		self.newToDo = [];

		self.addFunc = function addFunc() {
			self.newToDo.push({name: self.addToDo});
			self.addToDo = "";
		};

		self.delFunc = function delFunc(i) {
			self.newToDo.splice(i, 1);
		};

		self.doFunc = function doFunc() {
			console.log(self.view);
		};
	}

})();