angular.module("app", []);

(function() {
	angular.module("app").controller("MainCtrl", [MainCtrl]);

	function MainCtrl () {
		var self = this;

		self.textCount = "";

		

		self.countFunc = function countFunc() {
			var maxLength = 160;

			self.words = 0;
			self.simbols = 0;
			self.spaces = 0;
			self.remainder = 160;

			if(self.textCount.length) {
				self.words = self.textCount.split(" ").length;
				self.simbols = self.textCount.length;
				self.spaces = self.words - 1;
				self.remainder = maxLength - self.simbols;
			};

		};
	};
})();
