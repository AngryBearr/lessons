(function() {
	var a = {};

	a.ajax = function(obj) {
		var req = new XMLHttpRequest();

		if (obj.url === undefined || obj.url === "") {
			console.log("Error! Please specify URL");
			return;
		}
		if (obj.method  === undefined || obj.method === "") {
			console.log("Error! Please specify Method");
			return;
		}

		req.open(obj.method.toUpperCase(), obj.url);
		req.setRequestHeader("Content-type", "application/json");

		req.addEventListener("load", function() {
			obj.success(req.responseText);
		});

		req.addEventListener("error", function() {
			obj.error(req.response);
		});

		if(obj.data && obj.method.toUpperCase() !== "POST") {
			// if(obj.data.length) {
				req.send(JSON.stringify(obj.data));
			// }
			// else {
			// 	var someArr = [];
			// 	someArr.push(obj.data);
			// 	console.log(JSON.stringify(someArr));
			// 	req.send(JSON.stringify(someArr));
			// }
		}

		else if (obj.data && obj.method.toUpperCase() === "POST") {
			req.send(JSON.stringify(obj.data));
		}

		else {
			req.send();
		}
	}
	window.ajax = a.ajax;
})();