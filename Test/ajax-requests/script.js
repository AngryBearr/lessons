(function() {
	// 'use strict';

	var countTwo = mainCounter();
	var countEditForm = secondCounter();
	var countForBtns = thirdCounter();

	var r = GetAllDataAndRender();

	var e = new EventManager();
	var addNewInstanceButton = $.elem("#addNewEntry");
	var addFormTpl = "";

	var getData = {
		url: "http://localhost:3333/entry",
		method: "GET",
		data: "",
		success: function(text) {
			// console.log(text);
			r.setData(text);
			countForBtns();
		},
		error: function(text) {
			console.log(text);
		}
	}

	var getMainTpl = {
		url: "tpl.html",
		method: "GET",
		data: "",
		success: function(text) {
			// console.log(text);
			r.setTpl(text);
			countForBtns();
		},
		error: function(text) {
			console.log(text);
		}
	}

	var getAddForm = {
		url: "add-form.html",
		method: "GET",
		data: "",
		success: function(text) {
			// console.log(text);
			addFormTpl = text;
			e.trigger("readyToParse");
			e.trigger("readyToAdd");
		},
		error: function(text) {
			console.log(text);
		}
	}

	var getEditForm = {
		url: "edit-form.html",
		method: "GET",
		data: "",
		success: function(text) {
			// console.log(text);
			r.setTpl(text);
			countEditForm();
		},
		error: function(text) {
			console.log(text);
		}
	}

	var postAnEntry = {
		url: "http://localhost:3333/entry",
		method: "POST",
		data: "",
		success: function(text) {
			console.log(text);
			e.trigger("parsePage");
		},
		error: function(text) {
			console.log(text);
		}
	}

	var deleteAnEntry = {
		url: "http://localhost:3333/entry",
		method: "DELETE",
		data: "",
		success: function(text) {
			console.log(text);
		},
		error: function(text) {
			console.log(text);
		}
	}

	function mainCounter() {
		var c = 0;
		return function() {
			c++;
			if (c == 2) {
				e.trigger("renderPage");
				c = 0;
			}
		}
	}

	function secondCounter() {
		var c = 0;
		return function() {
			c++
			if(c == 2) {
				e.trigger("editFormComtplete");
				c = 0;
			}
		}
	}

	function thirdCounter() {
		var c = 0;
		return function() {
			c++
			if(c == 2) {
				e.trigger("deleteInit");
				e.trigger("editButtonInit");
				c = 0;
			}
		}
	}

	function GetAllDataAndRender() {
		this.tpl = "";
		this.data = "";

		this.setData = function (data) {
			this.data = JSON.parse(data);
			countTwo();
		}

		this.setTpl = function (tpl) {
			this.tpl = tpl;
			countTwo();
		}

		this.render = function (data, template) {
			var container = $.elem(".container");
			var res = "";

			ajax(data);
			ajax(template);

			e.regEvent("renderPage", function() {
				var parseTpl = new TemplateEngine();

				parseTpl.collection = r.data;
				parseTpl.template = r.tpl;
				parseTpl.render();

				res = parseTpl.getTemplate();

				container.innerHTML = res;
			});
		}
	}

	var r = new GetAllDataAndRender();



	r.render(getData, getMainTpl);


	function addButton() {
		var container = $.elem(".container");

		$.onclick("#addNewEntry", function() {
			this.style.display = "none";
			ajax(getAddForm);

			e.regEvent("readyToParse", function() {
				container.innerHTML = addFormTpl;

				$.onclick("#cancelAdd", function() {
					addNewInstanceButton.style.display = "block";
					e.trigger("parsePage");
				});
			});
		});
	}

	addButton();

	e.regEvent("readyToAdd", function() {
		function addNewEntry() {
			$.onclick("#addNewEntryButton", function() {

				addNewInstanceButton.style.display = "block";

				var sendObj = {
					Author: $.elem("#author").value,
					Title: $.elem("#title").value,
					Text: $.elem("#text").value
				}

				postAnEntry.data = sendObj;

				ajax(postAnEntry);
			});
		}
		addNewEntry();
	});


	e.regEvent("parsePage", function() {
		r.render(getData, getMainTpl);
	});


	e.regEvent("deleteInit", function() {
		function deleteButton() {
			$.onclick(".deleteEntry", function() {
				console.log(this.dataset.entryid);
				var delObj = {
					EntryId: this.dataset.entryid
				}
				deleteAnEntry.data = delObj;
				ajax(deleteAnEntry);
				r.render(getData, getMainTpl);
			});
		}
		deleteButton();
	});


	e.regEvent("editButtonInit", function() {
			$.onclick(".editEntry", function() {
				addNewInstanceButton.style.display = "none";

				var getEditBtnData = {
					url: "http://localhost:3333/entry/" + this.dataset.entryid,
					method: "GET",
					data: "",
					success: function(text) {
						// console.log(text);
						r.setData(text);
						countEditForm();
					},
					error: function(text) {
						console.log(text);
					}
				}

				r.render(getEditBtnData, getEditForm);

			});
	});


	e.regEvent("editFormComtplete", function() {
		$.onclick("#saveButton", function(){

			addNewInstanceButton.style.display = "block";
			var obj = {
				Author: $.elem("#author").value,
				Title: $.elem("#title").value,
				Text: $.elem("#text").value,
				EntryId: $.elem("#entryid").value
			}

			var editEntryRequest = {
				url: "http://localhost:3333/entry",
				method: "PUT",
				data: obj,
				success: function(text) {
					console.log(text);
					e.trigger("parsePage");
				},
				error: function(text) {
					console.log(text);
				}
			}

			ajax(editEntryRequest);
		});
		$.onclick("#cancelEdit", function() {
			addNewInstanceButton.style.display = "block";
			e.trigger("parsePage");
		});
	});

})();