(function() {
	var a = {};
	a.elem = function(elem) {
		if (document.querySelectorAll(elem).length <= 1) {
			return document.querySelector(elem);
		}
		else {
			return document.querySelectorAll(elem);
		}
	}

	a.onclick = function(elem, fun) {
		var elements = this.elem(elem);

		if(elements.length) {
			for(var i = 0; i < elements.length; i++) {
				elements[i].addEventListener("click", fun);
			}
		}
		else {
			elements.addEventListener("click", fun);
		}
	}

	window.$ = a;
})();