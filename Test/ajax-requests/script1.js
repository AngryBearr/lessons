(function() {


	var e = new EventManager();
	var tpl = "";
	var data = "";
	var counter = 0;
	var formCounter = 0;
	var formTpl = "";

	var addButton = document.querySelector("#addNewEntry");

	var container = document.querySelector(".container");

	function getTpl(url, success) {
		var gt = new XMLHttpRequest();
		gt.open("GET", url);

		gt.send();

		gt.addEventListener("load", function() {
			success(gt.responseText);
		});

		gt.addEventListener("error", function() {
			console.log("Error!!!");
		});

	}

	getTpl("tpl.html", writeTpl);

	function getData(url, success) {
		var gd = new XMLHttpRequest();
		gd.open("GET", url);

		gd.send();

		gd.addEventListener("load", function() {
			success(gd.responseText);
		});

		gd.addEventListener("error", function() {
			console.log("Error!!!");
		});

	}


	getData("http://localhost:3333/entry", writeData);


	function post(url, obj, postCallBack) {
		var sendObj = JSON.stringify(obj);

		var p = new XMLHttpRequest();
		p.open("POST", url);
		p.setRequestHeader("Content-type", "application/json");

		p.addEventListener("load", function() {
			console.log("Post Done!");
			postCallBack();
		});

		p.addEventListener("error", function() {
			console.log(p.response);
		});

		p.send(sendObj);

	}


	function put(url, obj, putCallBack) {
		var editObj = JSON.stringify(obj);

		var pt = new XMLHttpRequest();

		pt.open("PUT", url);
		pt.setRequestHeader("Content-type", "application/json");

		pt.addEventListener("load", function() {
			console.log(pt.responseText);
			putCallBack();
		});

		pt.addEventListener("error", function() {
			console.log(pt.response);
		});

		pt.send(editObj);
	}


	function deleteRquest(url, obj, deleteCallBack) {
		var delObj = JSON.stringify(obj);
		var d = new XMLHttpRequest();

		d.open("DELETE", url);
		d.setRequestHeader("Content-type", "application/json");

		d.addEventListener("load", function() {
			console.log(d.responseText);
			deleteCallBack();
		});

		d.addEventListener("error", function() {
			console.log(d.response);
		});

		d.send(delObj);
	}


	function writeTpl(text) {
		tpl = text;
		e.trigger("recievedData");
	}


	function writeData(text) {
		data = JSON.parse(text);
		e.trigger("recievedData");
	}

	function writeDataId(text) {
		data = [];
		data.push(JSON.parse(text));
		e.trigger("recievedData");
		e.trigger("editLoaded");
	}

	e.regEvent("recievedData", function() {
		counter ++;
		if (counter === 2) {
			var parseTpl = new TemplateEngine();

			parseTpl.collection = data;
			parseTpl.template = tpl;
			parseTpl.render();
			var res = parseTpl.getTemplate();
			container.innerHTML = res;
			deleteThatEntry();
			editThatEntry();
			counter = 0;
		}
	});


	function renderMainPage() {
		getTpl("tpl.html", writeTpl);
		getData("http://localhost:3333/entry", writeData);
	}


	function writeFormTpl(text) {
		formTpl = text;
		e.trigger("formTpl");
	}


	function addNewEntry() {

		addButton.addEventListener("click", function() {

			addButton.style.display = "none";

			getTpl("add-form.html", writeFormTpl);

			e.regEvent("formTpl", function() {
				formCounter++;

				if (formCounter === 1) {
					container.innerHTML = formTpl;

					var addNewEntryButton = document.querySelector("#addNewEntryButton");
					addNewEntryButton.addEventListener("click", addNew);
					formCounter = 0;
				}
				cancelAddButton();
			});
		});
	}

	addNewEntry();


	function addNew() {

		var obj = {
			Author: document.querySelector("#author").value,
			Title: document.querySelector("#title").value,
			Text: document.querySelector("#text").value
		}

		addButton.style.display = "block";

		post("http://localhost:3333/entry", obj, function() {
			e.trigger("newPage");
		});
	}

	e.regEvent("newPage", function() {
		renderMainPage();
	});


	function deleteThatEntry() {
		var deleteButtons = document.querySelectorAll(".deleteEntry");

		for (var i = 0; i < deleteButtons.length; i++) {
			deleteButtons[i].addEventListener("click", function() {
				var delObject = {
					EntryId: this.dataset.entryid
				}

				deleteRquest("http://localhost:3333/entry", delObject, function() {
					e.trigger("newPage");
				});

			});
		}
	}


	function editThatEntry() {
		var editButtons = document.querySelectorAll(".editEntry");

		for (var i = 0; i < editButtons.length; i++) {
			editButtons[i].addEventListener("click", function() {
				addButton.style.display = "none";

				getTpl("edit-form.html", writeTpl);
				getData("http://localhost:3333/entry/" + this.dataset.entryid, writeDataId);
			});
		}
	}

	e.regEvent("editLoaded", function() {
		saveChanges();
		cancelEditButton();
	});


	function saveChanges() {
		var saveButton = document.querySelector("#saveButton");

		saveButton.addEventListener("click", function() {
			var putObj = {
				Author: document.querySelector("#author").value,
				Title: document.querySelector("#title").value,
				Text: document.querySelector("#text").value,
				EntryId: document.querySelector("#entryid").value
			}

			addButton.style.display = "block";
			put("http://localhost:3333/entry", putObj, function() {
				e.trigger("newPage");
			});
		});
	}


	function cancelAndReturnButton() {
		addButton.style.display = "block";
		e.trigger("newPage");
	}


	function cancelEditButton() {
		var cancelEditButton = document.querySelector("#cancelEdit");

		cancelEditButton.addEventListener("click", cancelAndReturnButton);
	}


	function cancelAddButton() {
		var cancelAddButton = document.querySelector("#cancelAdd");

		cancelAddButton.addEventListener("click", cancelAndReturnButton);
	}

})();

// function GetAllData () {
// 	this.tpl = '';
// 	this.data = [];

// 	this.setData = function (data) {
// 		this.data = data;
// 	}

// 	this.setTpl = function (tpl) {
// 		this.tpl = tpl;
// 	}

// 	this.render = function () {

// 	}
// }

// var r = new GetAllData();
// r.setData();


// var someRandomObj = {
// 	Author: "Balodya Kortohinooooo",
// 	Title: "Baba Yagaaaa",
// 	Text: "Baba yaga, kostyanaya noga..."
// 	}

// 	var paramPost = {
// 		url: "http://localhost:3333/entry",
// 		method: "post",
// 		data: someRandomObj,
// 		success: function(text) {
// 			console.log(text);
// 		},
// 		error: function(text) {
// 			console.log(text);
// 		}
// 	}

// 	ajax(paramPost);