var list = {
			Author: "Vasya",
			Title: "Nau4nie trudy Vasiliya Lupu",
			Text: "Text is not defined"
};

var someObj = {
		EntryId: "EntryId_5579110c-d553-4daf-955f-c470fe06ce3a",
		Author: "Vasya",
		Title: "Nau4nie trudy Vasiliya Lupu",
		Text: "Text is not defined"
	}

var someOtherObj = {
		EntryId: "EntryId_973e9c0f-8da6-49b2-84d6-45488379fb25",
		Author: "Vasya",
		Title: "Nau4nie trudy Vasiliya Lupu",
		Text: "Text is not defined"
}


function get(url, func) {
	var getStr = "";

	var a = new XMLHttpRequest();

	a.open("GET", url);
	a.send();
	a.onreadystatechange = function() {
		if(a.readyState === 4 && a.status === 200) {
			getStr = a.responseText;
			console.log(getStr);
		}
	}
}

// get("http://localhost:3333/entry", resp);
// console.log(getStr);
function resp() {
	console.log("Success");
}


function post(url, obj, success, error) {
	var jsonObj = JSON.stringify(obj);
	var req = new XMLHttpRequest();

	req.open("POST", url)
	req.setRequestHeader("Content-Type", "application/json");
	req.send(jsonObj);

	req.onreadystatechange = function() {
		if(req.readyState === 4 && req.status === 200) {
			success();
		}
		else if(req.readyState !== 4 && req.status !== 200){
			error();
		}
	}
}


function suc() {
	console.log("Success");
}

function err() {
	console.log("Error");
}

// post("http://localhost:3333/entry", list, suc, err);



// post('', {author: 'sadca'}, function() {
// 	alert('ss')
// })

// post('', obj, suc, err)

// var toJSON1 = JSON.stringify(list);
// console.log(toJSON1);

function put(url, obj, success, error) {
	var jsonObj = JSON.stringify(obj);

	var r = new XMLHttpRequest();
	r.open("PUT", url);

	r.setRequestHeader("Content-Type", "application/json");
	r.send(jsonObj);


	r.onreadystatechange = function() {
		if (r.readyState === 4 && r.status === 200) {
			// console.log(r.responseText);
			success();
		}
		else if (r.readyState !== 4 && r.status !== 200) {
			error();
		}
	}
}



function s() {
	console.log("Successfully updated");
}

function e() {
	console.log("Some error occured while updating");
}


// put("http://localhost:3333/entry", someObj, s, e);


function del(url, obj, success, error) {
	var jsonObj = JSON.stringify(obj);

	d = new XMLHttpRequest();
	d.open("DELETE", url);

	d.setRequestHeader("Content-Type", "application/json");
	d.send(jsonObj);

	d.onreadystatechange = function () {
		if (d.readyState === 4 && d.status === 200) {
			console.log(d.responseText);
			success();
		}
		else if (d.readyState !== 4 && d.status !== 200) {
			error();
		}
	}
}

function su() {
	console.log("Successfully deleted");
}

function er() {
	console.log("Some error occured while deleting");
}


// del("http://localhost:3333/entry", someOtherObj, su, er);