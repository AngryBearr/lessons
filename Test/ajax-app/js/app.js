var buttons = document.querySelectorAll("button");
var clearButton = document.getElementById("clear");


function inner(text) {
	var container = document.querySelector(".container");
	container.innerHTML = text;
}


for (var i = 0; i < buttons.length; i++) {
	buttons[i].addEventListener("click", function () {
		var data = this.dataset.index;

		getTpl(data, function(text) {
			inner(text);
		});


	});
}


function getTpl(fileName, success) {
	var g = new XMLHttpRequest();
	g.open("GET", "tpls/" + fileName + ".html");

	g.onreadystatechange = function () {
		if(g.readyState === 4 && g.status === 200) {
			success(g.responseText);
		}
	}

	g.send();
}


clearButton.addEventListener("click", function() {
	inner("");
});